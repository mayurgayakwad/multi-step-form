import { useState, createContext } from "react";
const FormContext = createContext();

export const FormProvider = ({ children }) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLirstName] = useState("");
  const [dob, setDob] = useState("");
  const [emailAdd, setEmailAdd] = useState("");
  const [address, setAddress] = useState("");
  const [message, setMessage] = useState("");
  const [FirstNameerror, setFirstNameError] = useState(false);
  const [LastNameerror, setLastNameError] = useState(false);
  const [dobError, setDobError] = useState(false);
  const [emailAddError, setEmailAddError] = useState(false);
  const [addressError, setAddressError] = useState(false);
  const [messageError, setMessageError] = useState(false);
  const [checkSelected, setCheckSelected] = useState(1)
  const [ selectGender, setSelectGender ] = useState('')
  const [ genderError, setGenderError] = useState(false);
  return (
    <FormContext.Provider
      value={{
        firstName,
        lastName,
        dob,
        emailAdd,
        address,
        message,
        FirstNameerror,
        LastNameerror,
        dobError,
        emailAddError,
        addressError,
        messageError,
        checkSelected,
        selectGender,
        genderError,
        setFirstName,
        setLirstName,
        setDob,
        setEmailAdd,
        setAddress,
        setMessage,
        setFirstNameError,
        setLastNameError,
        setDobError,
        setEmailAddError,
        setAddressError,
        setMessageError,
        setCheckSelected,
        setSelectGender,
        setGenderError
      }}
    >
      {children}
    </FormContext.Provider>
  );
};

export default FormContext;
