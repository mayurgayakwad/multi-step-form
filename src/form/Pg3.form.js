import React, { useContext, useState } from "react";
import "../form/page3.css";
import FormContext from "../formContext/formcontext";
import PageHeaders from "./PageHeaders";
import ThirdPageImage from "../Images/ThirdPageImage.jpg";
export const Pg3 = (props) => {
  const { selectGender, setSelectGender } = useContext(FormContext);
  const { genderError, setGenderError } = useContext(FormContext);
  const validate = () => {
    if (!!selectGender) {
      setGenderError(false);
      return true;
    } else {
      setGenderError(true);
      return false;
    }
  };
  const onSubmits = (event) => {
    if (validate()) {
      event.preventDefault();
      alert("Form submitted");
      window.location.reload(false);
    }
  };
  return (
    <>
      <img className="image" src={ThirdPageImage} alt="image" />
      <div className="rightDiv">
        <PageHeaders currentPage={3} signUpPage={true} massagePage={true} />

        <h5>
          Step<span> 3/3</span>
        </h5>
        <h2>Checkbox</h2>
        <div>
          <div className="container">
            <div class="img_block">
              <label for="male">
                <img
                  src="https://media.istockphoto.com/id/1155391273/es/vector/hombre-en-el-cargo.jpg?s=1024x1024&w=is&k=20&c=AfOlkUkNunmYSCVd3i2_hBDRuAboUBuqXj-q1FXMwNY="
                  alt="media.istockphoto.com"
                  name="male"
                />
              </label>
              <input
                type="radio"
                id="male"
                checked={selectGender === "male"}
                className="check-input-on-image"
                name="sameCheck"
                onChange={() => setSelectGender("male")}
              />
            </div>
            <div class="img_block">
              <label for="female">
                <img
                  src="https://media.istockphoto.com/id/1155391253/es/vector/mujer-en-el-cargo.jpg?s=612x612&w=is&k=20&c=Jg4fe3hCvftVMVNggs5PYNTStMp68aKvTP7VwW4lFAs="
                  alt="vector"
                />
              </label>
              <input
                type="radio"
                id="female"
                checked={selectGender === "female"}
                className="check-input-on-image"
                name="sameCheck"
                onChange={() => setSelectGender("female")}
              />
            </div>
          </div>
          {genderError ? (
            <label
              style={{ color: "red", position: "relative", bottom: "20px" }}
            >
              *Please select Gender.
            </label>
          ) : (
            ""
          )}
          <div className="radio-btn">
            <input type="radio" id="one-page3" name="one" />
            <label for="one-page3">I want to add this option.</label>
            <br />
            <input type="radio" id="two-page3" name="one" checked="checked" />
            <label for="two-page3">
              Let me click on this checkbox and choose some cool stuff.
            </label>
          </div>
          <hr />
          <div className="buttonDiv">
            <button
              type="click"
              className="back-btn"
              onClick={() => {
                props.Back();
              }}
            >
              Back
            </button>
            <button type="submit" className="next-btn" onClick={onSubmits}>
              Submit
            </button>
          </div>
        </div>
      </div>
    </>
  );
};
