import React, { useContext, useState } from "react";
import FormContext from "../formContext/formcontext";
import "../form/page1.css";
import FirstPageImages from "../Images/FirstPageImage.jpg";
import PageHeaders from "./PageHeaders";

export const Pg1 = (props) => {
  const { firstName, setFirstName } = useContext(FormContext);
  const { lastName, setLirstName } = useContext(FormContext);
  const { dob, setDob } = useContext(FormContext);
  const { emailAdd, setEmailAdd } = useContext(FormContext);
  const { address, setAddress } = useContext(FormContext);
  const { FirstNameerror, setFirstNameError } = useContext(FormContext);
  const { LastNameerror, setLastNameError } = useContext(FormContext);
  const { dobError, setDobError } = useContext(FormContext);
  const { emailAddError, setEmailAddError } = useContext(FormContext);
  const { addressError, setAddressError } = useContext(FormContext);
  const [emailErrorMsg, setEmailErrorMsg] = useState('')
  
  const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;


  const FirstNameEvent = (event) => {
    setFirstName(event.target.value);
  };
  const LastNameEvent = (event) => {
    setLirstName(event.target.value);
  };
  const DobEvent = (event) => {
    setDob(event.target.value);
  };
  const EmailEvent = (event) => {
    setEmailAdd(event.target.value);
  };
  const AddressEvent = (event) => {
    setAddress(event.target.value);
  };

  const validate = (event) => {
    let flag = true;
    if (firstName.length === 0) {
      setFirstNameError(true);
      flag = false;
    } else {
      setFirstNameError(false);
    }
    if (lastName.length === 0) {
      setLastNameError(true);
      flag = false;
    } else {
      setLastNameError(false);
    }
    if (dob.length === 0) {
      setDobError(true);
      flag = false;
    } else {
      setDobError(false);
    }

    if (emailAdd.length === 0) {
      setEmailAddError(true);
      setEmailErrorMsg('*Email Address is required.')
      flag = false;
    } else if (!emailRegex.test(emailAdd)) {
      setEmailAddError(true);
      setEmailErrorMsg('*Input Valid Email Address')
      flag = false;
    } else {
      setEmailAddError(false);
    }

    if (address.length === 0) {
      setAddressError(true);
      flag = false;
    } else {
      setAddressError(false);
    }
    return flag;
  };

  return (
    <>
      <img className="image" src={FirstPageImages} alt="Image Not Found" />
      <div className="rightDiv">
        <PageHeaders currentPage={1} massagePage={2} checkboxPage={3} />
        <h5>
          Step<span> 1/3</span>
        </h5>
        <h2>Sign UP</h2>
        <div className="container-page1">
          <label for="name">First Name</label>
          <label for="name">&nbsp;&nbsp;&nbsp;Last Name</label>
        </div>
        <div className="inputs">
          <input
            className="input"
            name="fName"
            type="text"
            value={firstName}
            onChange={FirstNameEvent}
          />

          <input
            className="input"
            name="lName"
            type="text"
            value={lastName}
            onChange={LastNameEvent}
          />
        </div>
        {FirstNameerror ? (
          <label className="FirstNameError" for="name">
            *First Name is required.
          </label>
        ) : (
          ""
        )}
        <br />
        {LastNameerror ? (
          <label className="laNameError">*Last Name is required. </label>
        ) : (
          ""
        )}
        <br />
        <div className="container-page1">
          <label for="name">Date of Birth </label>
          <label for="name">Email Address</label>
        </div>
        <div className="inputs">
          <input
            className="input"
            name="Dob"
            type="date"
            defaultValue="1999-09-04"
            value={dob}
            onChange={DobEvent}
          />

          <input
            className="input"
            name="Email"
            type="email"
            value={emailAdd}
            onChange={EmailEvent}
          />
        </div>
        {dobError ? (
          <label className="FirstNameError" for="name">
            *Date of Birth is required.
          </label>
        ) : (
          ""
        )}
        <br />
        {emailAddError ? (
          <label className="EmailError" for="name">
            {emailErrorMsg}
          </label>
        ) : (
          ""
        )}
        <label className="addresslabel">Address</label>
        <input
          type="text"
          name="Address"
          className="addressinput"
          value={address}
          onChange={AddressEvent}
        />
        <br />
        {addressError ? (
          <p style={{ color: "red", margin: "0px" }}>*Address is required.</p>
        ) : (
          ""
        )}
        <br />
        <br /> <hr />
        <button
          onClick={() => {
            if (validate()) {
              props.Next();
            }
          }}
          className="next-btn"
        >
          Next Step
        </button>
      </div>
    </>
  );
};
