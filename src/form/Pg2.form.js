import React, { useContext } from "react";
import FormContext from "../formContext/formcontext";
import "../form/page2.css";
import secondPageImage from "../Images/SecondPageImage.jpg";
import PageHeaders from "./PageHeaders";

export const Pg2 = (props) => {
  const { message, setMessage } = useContext(FormContext);
  const { messageError, setMessageError } = useContext(FormContext);
  const { checkSelected, setCheckSelected } = useContext(FormContext);
  const MassageEvent = (event) => {
    setMessage(event.target.value);
  };

  const validate = () => {
    let flag = true;
    if (message.length < 20) {
      setMessageError(true);
      flag = false;
    } else {
      setMessageError(false);
    }
    return flag;
  };
  return (
    <>
      <img className="image" src={secondPageImage} alt="Image Not Found" />
      <div className="rightDiv">
        <PageHeaders currentPage={2} signUpPage={true} checkboxPage={3} />

        <h5>
          Step<span> 2/3</span>
        </h5>
        <h2>Message</h2>
        <label for="name" className="message">
          Message
        </label>
        <textarea
          className="text-area"
          value={message}
          onChange={MassageEvent}
        />
        {messageError ? (
          <label style={{ color: "red" }}>
            *Please enter at least 20 characters for the message.
          </label>
        ) : (
          ""
        )}
        <div className="choices-page2">
          <input
            type="radio"
            checked={checkSelected === 1}
            id="one-page2"
            name="sameCheck"
            value={"The number one choices"}
            onChange={() => setCheckSelected(1)}
          />
          <label for="one-page2" style={{ cursor: "pointer" }}>
            The number one choices
          </label>
          <input
            type="radio"
            id="two-page2"
            checked={checkSelected === 2}
            name="sameCheck"
            value={"The number two choices"}
            onChange={() => setCheckSelected(2)}
          />
          <label for="two-page2" style={{ cursor: "pointer" }}>
            The number two choices
          </label>
        </div>
        <hr />
        <div className="buttonDiv">
          <button
            type="click"
            className="back-btn"
            onClick={() => {
              props.Back();
            }}
          >
            Back
          </button>
          <button
            onClick={() => {
              if (validate()) {
                props.Next();
              }
            }}
            className="next-btn"
          >
            Next Step
          </button>
        </div>
      </div>
    </>
  );
};
