import React from "react";
import "../form/pageHeaders.css";

function PageHeaders({ currentPage, signUpPage, massagePage, checkboxPage }) {
  return (
    <>
      <div className="RightTopDiv">
        <div className={`box-page${currentPage} box1-page${currentPage}`}>
          {signUpPage || currentPage}
        </div>
        Sign Up
        <div className={`box-page${currentPage} box2-page${currentPage}`}>
          {massagePage || currentPage}
        </div>
        Massage
        <div className={`box-page${currentPage} box3-page${currentPage}`}>
          {checkboxPage || currentPage}
        </div>
        Checkbox
      </div>
      <br />
      <hr />
      <br />
    </>
  );
}

export default PageHeaders;
